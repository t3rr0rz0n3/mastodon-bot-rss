# MASTODON BOT RSS

Add to Mastodon from RSS.

# Packages

+ [Mastodon-api](https://www.npmjs.com/package/mastodon-api)
+ [RSS Parser](https://www.npmjs.com/package/rss-parser)
+ [File System](https://nodejs.org/api/fs.html)
+ [Dotenv](https://www.npmjs.com/package/dotenv)
* [Request](https://www.npmjs.com/package/request)

# Install MastodonBot RSS

Clone the repository:

```
git clone https://gitlab.com/t3rr0rz0n3/mastodon-bot-rss [NameBot]
```
And install packages:

```
npm install mastodon-api fs dotenv parse-rss request
```

First, configure your .env file with your **CLIENT_KEY**, **CLIENT_SECRET** and **ACCESS_TOKEN**.

```
cp .env-sample .env
```

Also, add the following parameters:

+ **API_URL**: Add your api url (por example, https://mynodemastodon.xyz/api/v1/)
+ **FEED**: Add the RSS feed of the page you want to share at Mastodon.
+ **FILE_LAST_DATE**: Add the name of the file where to save the last execution date.
+ **DELAY**: Time it will wait for the script to run. It must be put in milliseconds. 4h by default.
+ **TMP_PATH**: Temporary path where you will save the images.

Optionally, you can add hashtags to the toot separated by spaces:

+ **HASTAGS**: "#Bot #Fediverse #Mastodon"

Or add default image to toot:

+ **URL_IMAGE_DEFAULT**: "https://xarxa.cloud/image/default.png"

Now, you can execute the script with:

```
node mastodon-bot-rss.js
```

# Running your bot with SystemD

I recommend adding the bots under the same directory where the Mastodon installation is located.

Copy mastodon-bot-rss.service to /lib/systemd/system and change:

+ [USER]: By the user who is currently running mastodon.
+ [PATH_TO_BOT]: By the path where the bot is located, for example:

```
ExecStart=/usr/bin/node -r /home/mastodon/bots/namebot/node_modules/dotenv/config.js /home/mastodon/bots/namebot/mastodon-bot-rss.js dotenv_config_path=/home/mastodon/bots/namebot/.env
```

Save file and close. We reloaded SystemD:

```
systemctl daemon-reload
```

And finally, launch our bot with:

```
systemctl start mastodon-bot-rss.service
```

And if you want the service to start at start-up:

```
systemtl enable mastodon-bot-rss.service
```

# License

GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

Mastodon-bot-rss
Copyright (C) 2020 by T3rr0rZ0n3
