require('dotenv').config();
const Mastodon = require('mastodon-api');
let Parser = require('rss-parser');
const fs = require('fs');
const request = require('request');

const M = new Mastodon({
    client_key: process.env.CLIENT_KEY,
    client_secret: process.env.CLIENT_SECRET,
    access_token: process.env.ACCESS_TOKEN,
    timeout_ms: 60 * 1000,
    api_url: process.env.API_URL,
});

const init_date = new Date('1970-01-01');

setInterval(startBot, process.env.DELAY);

function startBot() {
    console.log("Starting bot...");

    if (checkIfFileDateExecutionExists()) {
        parseRSS();
    } else {
        logger.error("No existe el fichero");
        return 0;
    }
}

function parseRSS() {

    let parser = new Parser();

    (async () => {
    
        let parse_feed_rss = await parser.parseURL(process.env.FEED);
        var last_execution_date = fs.readFileSync(process.env.FILE_LAST_DATE, 'utf8').substring(1,25);

        parse_feed_rss.items.forEach(post => {            

            title_post = post.title + ' ';
            link_post = post.link + ' ';
            first_category_post = getfirstCategoryPost(post);
            image_post_url = getURLImagePost(post['content:encoded']);
            custom_hastags = process.env.HASTAGS;

            if (checkDateToPost(last_execution_date, post.isoDate)) {
                console.log(title_post + link_post + first_category_post + custom_hastags);
                sendToMastodon(title_post + link_post + first_category_post + custom_hastags, image_post_url);
            }
        });
        addLastDateExecution();
    })();
}

function checkIfFileDateExecutionExists() {

    if (!fileExists(process.env.FILE_LAST_DATE)) {
        fs.createWriteStream(process.env.FILE_LAST_DATE,{mode:0o644});
        fs.writeFile(process.env.FILE_LAST_DATE, JSON.stringify(init_date, null, 2), function (err) {
            if (err) throw err;
        });
    }
    return true;
}

function addLastDateExecution() {

    if (fileExists(process.env.FILE_LAST_DATE)) {
        const now_date = new Date();
        fs.writeFileSync(process.env.FILE_LAST_DATE, JSON.stringify(now_date, null, 2));
    } 
}

function fileExists(path_from_file) {

    if (fs.existsSync(path_from_file)) {
        return true;
    }
}

function checkDateToPost(last_execution_date, post_publication_date) {

    if (post_publication_date > last_execution_date) {
        return true;      
    }
}

function getfirstCategoryPost(post) {
    
    if (post.categories) {
        return '#' + post.categories[0].replace(/\s/g, '') + ' ';
    }
    return "";
}

function getURLImagePost(content_post) {

    is_image_content = content_post.startsWith('<img');
    image_content_to_array = content_post.split('"');

    if (is_image_content) {
        image_url = image_content_to_array[1];
        return image_url;
    }
    return process.env.URL_IMAGE_DEFAULT;    
}

function downloadImageInPath(full_url_image) {
    
    var name_file_image = full_url_image.substring(full_url_image.lastIndexOf("/"), full_url_image.length);
    
    var download = function(full_url_image, name_file_image, callback){
        request.head(full_url_image, function(err, res, body){ 
        request(full_url_image).pipe(fs.createWriteStream(name_file_image)).on('close', callback);
        });
    };
    
    download(full_url_image, process.env.TMP_PATH + name_file_image, function(){
        console.log("Se descarga");
    });
    
    return process.env.TMP_PATH + name_file_image;
}

async function sendToMastodon(toot, image_post_url) {

    const path_local_image = downloadImageInPath(image_post_url);

    await new Promise(resolve => setTimeout(resolve, 2000));
    
    const stream = fs.createReadStream(path_local_image);
 
    const uploadParams = {
        file: stream
    }
    const uploadResponde = await M.post('media', uploadParams);
    const mediaId = uploadResponde.data.id;

    const tootParams = {
        status: toot,
        media_ids: [mediaId]
    }
    
    await M.post('statuses', tootParams, (error, data) => {

        if(error) {
            console.error(error);
        } else {
            console.log("Success, id: " + data.id + " " + data.created_at);
            fs.unlinkSync(path_local_image);
        }
    });
}
